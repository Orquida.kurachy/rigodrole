using EODE.Wonderland;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class GameEnding : StateBehaviour
{
    Machine _machine => (Machine)Machine;

    //temprorary, feed the scene with the manager
    public SceneField GameScene;

    public override Task Enter()
    {
        ExitGame();
        return base.Enter();
    }

    public override Task Exit()
    {
        return base.Exit();
    }

    private void ExitGame()
    {
        //Hide this shit with a canvas (and destroy it after)
        GameManager.Instance.LoadScene(GameScene).WrapErrors();
    }
}
