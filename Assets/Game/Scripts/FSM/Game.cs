using EODE.Wonderland;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class Game : StateBehaviour
{
    Machine _machine => (Machine)Machine;

    public override Task Enter()
    {
        return base.Enter();
    }

    public override Task Exit()
    {
        return base.Exit();
    }
}
