using EODE.Wonderland;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class GameManager : MonoSingleton<GameManager>
{
    protected override bool NeverReplace => true;

    public int CurrentGame { get; protected set; }

    protected override void Awake()
    {
        base.Awake();
        if (Instantiated && Instance != this) return;
    }

    public async Task LoadScene(SceneField sceneField, bool additive = true)
    {
        await sceneField.Load(additive);
    }

    public async Task CloseScene()
    {
        await UnityEngine.SceneManagement.SceneManager.UnloadSceneAsync(UnityEngine.SceneManagement.SceneManager.GetActiveScene());
    }

}
