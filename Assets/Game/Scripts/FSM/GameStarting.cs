using EODE.Wonderland;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class GameStarting : StateBehaviour
{
    Machine _machine => (Machine)Machine;

    //temprorary, feed the scene with the manager
    public SceneField GameScene;

    public override Task Enter()
    {
        InitGame();
        return base.Enter();
    }

    public override Task Exit()
    {
        return base.Exit();
    }

    private void InitGame()
    {
        //Hide this shit with a canvas (and destroy it after)
         GameManager.Instance.LoadScene(GameScene).WrapErrors();
    } 
}
