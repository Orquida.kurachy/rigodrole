using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;

public class TestDrag : MonoBehaviour
{
    private Main _controls;
    private Vector2 startPosition;
    private Vector2 endPosition;

    // Start is called before the first frame update
    void Start()
    {
        _controls = new Main();
        _controls.MainMap.Enable();
        _controls.MainMap.Pointer.performed += OnHold;
        _controls.MainMap.Pointer.started += OnStart;
        _controls.MainMap.Pointer.canceled += OnCancel;
    }

    private void OnCancel(InputAction.CallbackContext obj)
    {
        Debug.Log("pouet");
    }

    private void OnStart(InputAction.CallbackContext obj)
    {
        startPosition = Camera.main.ScreenToWorldPoint(Pointer.current.position.ReadValue());
    }

    public void OnHold(InputAction.CallbackContext context)
    {
        endPosition = Camera.main.ScreenToWorldPoint(Pointer.current.position.ReadValue());
        

        Vector2 movement = endPosition - startPosition;
        startPosition = endPosition;

        Debug.Log(movement);

        transform.Translate(movement);
        

    }
}